#include "vkUtils.h"

// check if the result is successful; if not, throw exception
void CheckResult(VkResult result) {
	if (result != VK_SUCCESS) {
		throw std::runtime_error("runtime failure, unsuccessful VkResult");
	}
}

// create the debugging messenger
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
	// get the process of the debug messenger
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	// if there is a process, return messenger
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

// destroy the debug messenger
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
	// get the process of the messenger and destroy it if it exists
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}

// find the supported format from a list of candidates
VkFormat findSupportedFormat(VkPhysicalDevice& physicalDevice, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
	// loop through the available formats
	for (VkFormat format : candidates) {
		// get the format properties from the physical device
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
		// if we need linear tiling and the properties from the physical
		// device contain the required features, return this format
		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		}
		// if we need optimal tiling and the properties from the physical
		// device contain the required features, return this format
		else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}
	// error if there is no supported format
	throw std::runtime_error("failed to find supported format");
}

// get swap chain support 
SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR& surface) {
	SwapChainSupportDetails details;
	// get the capabilities from the physical device
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

	// get the number of formats from physical device
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
	// if there is at least a format
	if (formatCount != 0) {
		// resize the formats field of the details and get the formats
		// from the physical device
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
	}

	// get the number of present modes
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	// if there is at least one present mode
	if (presentModeCount != 0) {
		// resize the presentModes field and store the presentModes
		// from the physical device
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}
	// return the details
	return details;
}

//  create a shader module from chars
VkShaderModule createShaderModule(const std::vector<char>& code, VkDevice& device) {
	// create info for the shader module with code's size and data
	VkShaderModuleCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	// create the shader module and check if creation is successful
	VkShaderModule shaderModule;
	if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
		throw std::runtime_error("failed to create shader module");
	}
	return shaderModule;
}

// rate a device's suitability
int rateDeviceSuitability(VkPhysicalDevice& device) {
	VkPhysicalDeviceProperties deviceProperties;
	VkPhysicalDeviceFeatures deviceFeatures;
	// get the device's properties and features
	vkGetPhysicalDeviceProperties(device, &deviceProperties);
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	// compute the score of the device
	int score = 0;

	// add a high score if it is a discrete gpu
	if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
		score += 1000;
	}
	// add the maximum size of the image2d
	score += deviceProperties.limits.maxImageDimension2D;

	// if the geometry shader is not available, the score becomes 0,
	// we need geometry for drawing on the screen
	if (!deviceFeatures.geometryShader) {
		return 0;
	}

	return score;
}

// check if validation layers are supported
bool checkValidationLayerSupport() {
	uint32_t layerCount;
	// get the number of layer properties
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
	// get the actual layer properties
	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());
	// loop through the validation layers - only 1
	for (const char* layerName : validationLayers) {
		bool layerFound = false;
		// loop through the available layers
		for (const auto& layerProperties : availableLayers) {
			// if the name is the same, the validation layer is found
			if (strcmp(layerName, layerProperties.layerName) == 0) {
				layerFound = true;
				break;
			}
		}
		if (!layerFound) {
			return false;
		}
	}
	return true;
}

// create a command buffer for a single time command
VkCommandBuffer beginSingleTimeCommands(VkDevice& device, VkCommandPool& commandPool) {
	// allocate info for the command buffer, using given command pool
	VkCommandBufferAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;

	// allocate the command buffer
	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

	// begin info for the command buffer that is an one time submit
	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	// begin the command buffer and return it
	vkBeginCommandBuffer(commandBuffer, &beginInfo);
	return commandBuffer;
}

// check if a format has is compatible with a stencil
bool hasStencilComponent(VkFormat format) {
	// check for at least a size of 24 bits
	return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

// create a texture sampler
void createTextureSampler(VkPhysicalDevice& physicalDevice, VkDevice& device, VkSampler& sampler) {
	// sampler info for the image, with repeating address mode and linear filter, and anisotropy
	VkSamplerCreateInfo samplerInfo{};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;

	// get the properties of the device
	VkPhysicalDeviceProperties properties{};
	vkGetPhysicalDeviceProperties(physicalDevice, &properties);
	// set sampler info regarding the physical properties
	samplerInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
	samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

	// create the sampler and check for success
	if (vkCreateSampler(device, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler");
	}
}

// create an image view
VkImageView createImageView(VkDevice& device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags) {
	// create the 2d view info, with given image and format
	VkImageViewCreateInfo viewInfo{};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	// set the aspect mask as the given flags, no mip map
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	// create the image view and check fro success
	VkImageView imageView;
	if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image view");
	}
	return imageView;
}

// choose the swap surface format from the available ones
VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
	// loop through the available formats
	for (const auto& availableFormat : availableFormats) {
		// check if the format and colorspace are as we require and return if so
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}
	// return the first one as backup
	return availableFormats[0];
}

// end a single time command buffer
void endSingleTimeCommands(VkCommandBuffer& commandBuffer, VkQueue& queue, VkDevice& device, VkCommandPool& commandPool) {
	// end the command buffer
	vkEndCommandBuffer(commandBuffer);
	// submit the command buffer
	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	// submit to queue and wait
	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(queue);
	// free the command buffer memory
	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

// copy buffer
void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkDevice& device, VkQueue& queue, VkCommandPool& commandPool) {
	// create a command buffer for copy command
	VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);
	// copy the given size of the buffer
	VkBufferCopy copyRegion{};
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	// end the command buffer
	endSingleTimeCommands(commandBuffer, queue, device, commandPool);
}

// find the memory type of a physical device
uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, VkPhysicalDevice& physicalDevice) {
	// get the properties of the physical device
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

	// loop through the property types
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i) {
		// if the current memory type satisfies the type filter and the properties required
		if (typeFilter && (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}
	// error if no satisfactory memory type has been found
	throw std::runtime_error("failed to find suitable memory type");
}

// create a buffer of given size and usage
void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkDevice& device, VkPhysicalDevice& physicalDevice) {
	// buffer info structure with given sizes and usage
	VkBufferCreateInfo bufferInfo{};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	// check if creation has been successful
	if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer");
	}
	// get the memory requirements for the buffer
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);
	// allocation information for the memory, with computed memory requirements
	// and the required memory type
	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties, physicalDevice);

	// try to allocate the memory and check the result
	if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate buffer memory");
	}
	// bind the memory with the buffer
	vkBindBufferMemory(device, buffer, bufferMemory, 0);
}

// create a vertex buffer
void createVertexBuffer(std::vector<Vertex>& vertices, VkDevice& device, VkPhysicalDevice& physicalDevice, VkBuffer& vertexBuffer, VkDeviceMemory& vertexBufferMemory, VkCommandPool& commandPool, VkQueue& queue) {
	// compute the size of the buffer size
	VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

	// staging buffer creation
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory, device, physicalDevice);

	// host memory for mapping and copying
	void* data;
	vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(device, stagingBufferMemory);

	// create a buffer for transfer and vertex bit usage for the vertices
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory, device, physicalDevice);

	// copy the data from the staging buffer to the vertex buffer
	copyBuffer(stagingBuffer, vertexBuffer, bufferSize, device, queue, commandPool);
	// destroy the staging buffer and free its memory
	vkDestroyBuffer(device, stagingBuffer, nullptr);
	vkFreeMemory(device, stagingBufferMemory, nullptr);
}

// find queue families for the device and surface
QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR& surface) {
	QueueFamilyIndices indices;
	// query the number of queue families
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	// store the queried queue families
	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);

	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	// loop through the queue families
	for (const auto& queueFamily : queueFamilies) {
		// if the queue is a graphics queue
		if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			// save its index for the queueFamily structure
			indices.graphicsFamily = i;
			// get whether the surface has present support
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
			// if there is present support, save the index in the present family as well
			if (presentSupport) {
				indices.presentFamily = i;
			}
			// if both indices have been set, no need to check other queue families
			if (indices.isComplete()) {
				break;
			}
		}
		i++;
	}
	// return the queue family struct
	return indices;
}

// get the required extensions
std::vector<const char*> getRequiredExtensions() {
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	// get the number of extensions
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
	// create the extensions vector
	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
	// add the validation layer extension
	if (enableValidationLayers) {
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}
	return extensions;
}

// check is the required extension is supported
bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
	// get the number of extensions available
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
	// get and save the available extensions
	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());
	// use a set to check if the available extensions include the required ones
	std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

	// loop through the available extensions and erase them from the required
	for (const auto& extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	// return true if all the required extensions are also available
	return requiredExtensions.empty();
}

// choose a swap present mode
VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
	// loop through the available present modes
	for (const auto& availablePresentMode : availablePresentModes) {
		// prefer the mailbox present and return if found
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		}
	}
	// if mailbox not found, return fifo
	return VK_PRESENT_MODE_FIFO_KHR;
}

// read a file in and return a vector of chars
std::vector<char> readFile(const std::string& filename) {
	// open the binary file
	std::ifstream file(filename, std::ios::ate | std::ios::binary);
	// is the file did not open, error
	if (!file.is_open()) {
		throw std::runtime_error("failed to open file");
	}
	// compute the filename
	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);
	// start from the beginning of the file
	file.seekg(0);
	// read in the entire file
	file.read(buffer.data(), fileSize);
	// close the file
	file.close();
	// return the read in buffer
	return buffer;
}

// load an obj model
void loadModel(std::vector<Vertex>& vertices, std::vector<uint32_t>& indices, UniformBufferObject& defaultUBO) {
	// create variables to hold the model from tinyobj
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	// load the model using tinyobj
	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, MODEL_PATH.c_str(), MATERIAL_PATH.c_str())) {
		throw std::runtime_error(warn + err);
	}
	// set the colours from the mtl file
	defaultUBO.ambientColor = glm::vec4(materials[0].ambient[0], materials[0].ambient[1], materials[0].ambient[2], 1.0f);
	defaultUBO.diffuseColor = glm::vec4(materials[0].diffuse[0], materials[0].diffuse[1], materials[0].diffuse[2], 1.0f);
	defaultUBO.specularColor = glm::vec4(materials[0].specular[0], materials[0].specular[1], materials[0].specular[2], materials[0].shininess);

	// create a map for the vertices to keep them uniquely stored
	std::unordered_map<Vertex, uint32_t> uniqueVertices{};
	// midpoint to centre the object
	glm::vec3 midPoint = glm::vec3(0.0f, 0.0f, 0.0f);
	// bounds of the object for scaling
	float xMin, xMax, yMin, yMax, zMin, zMax;
	xMin = yMin = zMin = 1000.0f;
	xMax = yMax = zMax = -1000.0f;

	// loop through shapes (i.e. face)
	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			Vertex vertex{};
			// get the vertex position
			vertex.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};
			// get the vertex texture coordinate
			vertex.texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};
			// set the default color
			vertex.color = { 1.0f, 1.0f, 1.0f };
			// get the vertex normal
			vertex.normal = {
				attrib.normals[3 * index.normal_index + 0],
				attrib.normals[3 * index.normal_index + 1],
				attrib.normals[3 * index.normal_index + 2]
			};

			// if the vertex has not been encountered before
			if (uniqueVertices.count(vertex) == 0) {
				// add it to the map
				uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
				// add it to the vertices
				vertices.push_back(vertex);
				// update the midpoint
				midPoint += vertex.pos;
				// update the bounds on all three axes
				if (vertex.pos.x < xMin) {
					xMin = vertex.pos.x;
				}
				if (vertex.pos.y < yMin) {
					yMin = vertex.pos.y;
				}
				if (vertex.pos.z < zMin) {
					zMin = vertex.pos.z;
				}

				if (vertex.pos.x > xMax) {
					xMax = vertex.pos.x;
				}
				if (vertex.pos.y > yMax) {
					yMax = vertex.pos.y;
				}
				if (vertex.pos.z > zMax) {
					zMax = vertex.pos.z;
				}
			}
			// add the indices
			indices.push_back(uniqueVertices[vertex]);
		}
	}
	// set the midpoint as the average
	midPoint /= vertices.size();
	// compute the maximum range
	float range = std::max(xMax - xMin, std::max(yMax - yMin, zMax - zMin));

	// update the vertices to be at the midpoint and in the range
	for (size_t i = 0; i < vertices.size(); ++i) {
		vertices[i].pos -= midPoint;
		vertices[i].pos /= range;
	}
}

// create an index buffer from given data
void createIndexBuffer(std::vector<uint32_t> indices, VkDevice& device, VkPhysicalDevice& physicalDevice, VkBuffer& indexBuffer, VkDeviceMemory& indexBufferMemory, VkCommandPool& commandPool, VkQueue& queue) {
	// compute the buffer size
	VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

	// create a staging buffer for the data
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory, device, physicalDevice);

	// host-visible memory for mapping
	void* data;
	vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices.data(), (size_t)bufferSize);
	vkUnmapMemory(device, stagingBufferMemory);

	// create the index buffer with its required usage
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory, device, physicalDevice);

	// copy data from the staging buffer to the index buffer
	copyBuffer(stagingBuffer, indexBuffer, bufferSize, device, queue, commandPool);

	// destroy the staging buffer and free the memory
	vkDestroyBuffer(device, stagingBuffer, nullptr);
	vkFreeMemory(device, stagingBufferMemory, nullptr);
}

// copy a buffer to an image
void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue) {
	// start a command buffer for copying
	VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);
	// region of the image to be copied into
	VkBufferImageCopy region{};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;
	// coloured image, no mip-map, no offset and given size of image
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = { 0, 0, 0 };
	region.imageExtent = { width, height, 1 };

	//copy the buffer to image
	vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
	// end the command buffer
	endSingleTimeCommands(commandBuffer, queue, device, commandPool);
}

// create an image
void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory, VkDevice& device, VkPhysicalDevice& physicalDevice) {
	// image information - 2d, of given size, format and tiling, no mip-map
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	// create the image and check for a success
	if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image");
	}

	// get the memory requirements
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device, image, &memRequirements);
	// create the allocation information with computed requirements and memory type
	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, physicalDevice);
	// allocate the memory and check for success
	if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate image memory");
	}
	// bind the image and memory
	vkBindImageMemory(device, image, imageMemory, 0);
}

// transition an image layout
void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue) {
	// start a command buffer for copying
	VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);
	// barrier for the memory to protect the image while transitioning
	VkImageMemoryBarrier barrier{};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	// set the image, no mip map
	barrier.image = image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	// pipeline stage flags to save the stages for different transitions
	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;

	// if the image goes from undefined to transfer destination
	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		// wait for top of pipe (the beginning) to end before starting to transition 
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		// stage transfer bit has to wait for transition ot end
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	// else if the image goes from transfer dest to shader read
	else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		// wait for stage transfer bit to end to start transitioning
		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		// fragment shader cannot start until transition ends
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else {
		throw std::invalid_argument("unsupported layout transition");
	}

	// set the barrier
	vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	// end the command
	endSingleTimeCommands(commandBuffer, queue, device, commandPool);
}

// check if the device is suitable
bool isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR& surface) {
	// find the queue family indices
	QueueFamilyIndices indices = findQueueFamilies(device, surface);
	// check if the extensions are supported
	bool extensionSupported = checkDeviceExtensionSupport(device);
	bool swapChainAdequate = false;

	// if the extensions are supported
	if (extensionSupported) {
		// check id the swapchain present mode and format are also supported
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device, surface);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	// get the supported features
	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures(device, &supportedFeatures);
	// return true if there are both a graphics and a present queue, all
	// the extensions are supported, swapchain is supported and
	// samplerAnisotropy exists
	return indices.isComplete() && extensionSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;
}