#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (binding = 0) uniform sampler2D fontSampler;

layout (location = 0) in vec2 inUV;
layout (location = 1) in vec4 inColor;

layout (location = 0) out vec4 outColor;

void main() 
{
	float gamma = 2.2f;
	outColor = inColor * texture(fontSampler, inUV);
	outColor.rgb = pow(outColor.rgb, vec3(gamma));	
}