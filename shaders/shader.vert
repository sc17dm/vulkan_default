#version 450
#extension GL_ARB_separate_shader_objects : enable

// huge UBO struct for the model information, what rendering
// state are we in and light and colour information
layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
    mat4 normal;
    vec4 lightPos;
    vec4 states;
    vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
} ubo;

// we get the position, colour, normal and texcoord
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

// we need to pass the color, normal, light and view,
// material properties and what we need to show
layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 viewDir;
layout(location = 3) out vec3 lightDir;
layout(location = 4) out vec3 states;
layout(location = 5) out vec3 ambient;
layout(location = 6) out vec3 diffuse;
layout(location = 7) out vec3 specular;
layout(location = 8) out vec2 fragTexCoord;
layout(location = 9) out float shininess;


void main() {
    // pass the colour, texture directly
    fragColor = inColor;
    fragTexCoord = inTexCoord;
    // multiply the normal by its matrix
    fragNormal = mat3(ubo.normal) * inNormal;
    
    // compute the model view
    mat4 modelView = ubo.view * ubo.model; 
    // compute the position of the vertex
    gl_Position = ubo.proj * modelView * vec4(inPosition, 1.0);
    
    // compute the fragment position
    vec4 fragPos = modelView * vec4(inPosition, 1.0f);
    // get the view direction for light computation
    viewDir = -vec3(normalize(fragPos));
    // compute the light position and direction
    vec4 light = ubo.view * ubo.lightPos;
    lightDir = vec3(normalize(light - fragPos));

    // pass the states and the material properties
    states = ubo.states.xyz;
    ambient = ubo.ambientColor.xyz;
    diffuse = ubo.diffuseColor.xyz;
    specular = ubo.specularColor.xyz;
    shininess = ubo.specularColor.w;
}