#version 450
#extension GL_ARB_separate_shader_objects : enable
// receive the fragment colour, normal, material properties
layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 viewDir;
layout(location = 3) in vec3 lightDir;
layout(location = 4) in vec3 states;
layout(location = 5) in vec3 ambient;
layout(location = 6) in vec3 diffuse;
layout(location = 7) in vec3 specular;
layout(location = 8) in vec2 fragTexCoord;
layout(location = 9) in float shininess;
// pass the final colour
layout(location = 0) out vec4 outColor;
// texture to sample from
layout(binding = 1) uniform sampler2D texSampler;

void main() {
    // if the first checkbox is not set - no rendering
    if(states.x < 0.5f){
        discard;
    }
    // default light colour
    vec4 lightColour = vec4(5.0f, 5.0f, 5.0f, 1.0f);
    // compute the sampled texture colour
    vec4 color = texture(texSampler, fragTexCoord);
    // normalise the normal
    vec3 normal = normalize(fragNormal);
    // compute the diffuse coefficient
    float diffuseCoeff = max(dot(normal, lightDir), 0.0f);
    // get the reflected direction of the light
    vec3 reflectDir = reflect(-lightDir, normal);
    // compute the specular coefficient
    float specularCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), shininess);
    // set default out colour as white
    outColor = vec4(1.0f);
    // compute the ambient and diffuse contribution; default ambient coefficient
    vec4 ambientColor = vec4(ambient, 1.0f);
    vec4 diffuseColor = vec4(diffuse, 1.0f);
    float ambientCoeff = 0.2f;

    // second checkbox is on, we need to add texture
    if(states.y > 0.5f){
        // add the texture to the final output
        outColor = outColor * color;
        // we know that the ambient and diffuse have a texture as well
        ambientColor = ambientColor * color;
        diffuseColor = diffuseColor * color;
    }
    // if the second checkbox is not on, we don't need texture
    else{
        color = outColor;
    }

    // if the third checkbox is on, we need Phong shading
    if(states.z > 0.5f){
        // add all the required components for the Phong shading
        outColor = ambientCoeff * ambientColor + (diffuseCoeff * diffuseColor * color + specularCoeff * vec4(specular, 1.0f)) * lightColour;
    }

} 