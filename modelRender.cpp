#pragma once
#include"vkUtils.h"
#include "modelRender.h"

// initialize the glfw window
void ModelRender::initWindow() {
    glfwInit();
    // use no helper functions from glfw
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    // create the window
    window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
    // link the window and this class
    glfwSetWindowUserPointer(window, this);
    // use the helper imgui function to initialize imgui
	ImGui_ImplGlfw_InitForVulkan(window, true);
    // set the callback function for resizing the window
    glfwSetFramebufferSizeCallback(window, frameBufferResizeCallback);
}

// callback function for resizing the window
void ModelRender::frameBufferResizeCallback(GLFWwindow* window, int width, int height) {
    // get the current class' pointer
    auto app = reinterpret_cast<ModelRender*>(glfwGetWindowUserPointer(window));
    // set the flag that we use to resize the buffers
    app->frameBufferResized = true;
}

// initialize the vulkan resources
void ModelRender::initVulkan() {
    // create the vulkan instance
    createInstance();
    // create and setup the debug messenger for validation
    setupDebugMessenger();
    // create the khr surface to present to
    createSurface();
    // pick an adequate physical device
    pickPhysicalDevice();
    // create a logical device
    createLogicalDevice();
    // set the pointers of the device and physical device for UI
	imguiRender.device = &device;
	imguiRender.physicalDevice = &physicalDevice;
    // create the swapchain
    createSwapChain();
    // create image views for the swapchain
    createImageViews();
    // create a render pass
    createRenderPass(renderPass);
    // create the layout for the descriptor set
    createDescriptorSetLayout();
    // create the graphics pipeline
    createGraphicsPipeline();
    // create the depth resources
    createDepthResources();
    // create the framebuffers
    createFramebuffers();
    // create the command pool
    createCommandPool();
    // load the obj model
    loadModel(vertices, indices, defaultUBO);
    // create the vertex buffer for the model
    createVertexBuffer(vertices, device, physicalDevice, vertexBuffer, vertexBufferMemory, commandPool, graphicsQueue);
    // create the index buffer for the model
    createIndexBuffer(indices, device, physicalDevice, indexBuffer, indexBufferMemory, commandPool, graphicsQueue);
    // create uniform buffers
    createUniformBuffers();
    // create the descriptor pool
    createDescriptorPool();
    // create the texture for the model, view and sampler
    createTextureImage();
    createTextureImageView();
    createTextureSampler(physicalDevice, device, textureSampler);
    // create the descriptor sets
    createDescriptorSets();
    // set the queue and command pool pointers for UI
	imguiRender.queue = graphicsQueue;
	imguiRender.commandPool = commandPool;
    // prepare the resources and pipeline for UI
	imguiRender.prepareResources();
	imguiRender.preparePipeline(VK_NULL_HANDLE, renderPass);
    // create synchronization objects
    createSyncObjects();
    // create the command buffers
    createCommandBuffers();
}

// create the depth resources
void ModelRender::createDepthResources() {
    // find the available depth format
    VkFormat depthFormat = findDepthFormat();
    // create an depth image with depth usage
    createImage(swapChainExtent.width, swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory, device, physicalDevice);
    // create the image view for the depth image
    depthImageView = createImageView(device, depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);
}

// find if depth format is supported: need 32 bits in size, optimal
// tiling and usage as a depth stencil
VkFormat ModelRender::findDepthFormat() {
    return findSupportedFormat(physicalDevice, { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT }, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

// create a texture image view
void ModelRender::createTextureImageView() {
    textureImageView = createImageView(device, textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT);
}

// create a texture image 
void ModelRender::createTextureImage() {
    int texWidth, texHeight, texChannels;
    // read in the data of the texture image using stbi
    stbi_uc* pixels = stbi_load(TEXTURE_PATH.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    // compute the image size
    VkDeviceSize imageSize = texWidth * texHeight * 4;
    // if there is no data, error
    if (!pixels) {
        throw std::runtime_error("failed to load the texture image");
    }

    // use a staging buffer as temporary storage to use better memory n the device
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    // create the staging buffer
    createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory, device, physicalDevice);

    // host memory, map to staging buffer and unmap
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(device, stagingBufferMemory);
    // free the read in data because it is already saved in staging buffer
    stbi_image_free(pixels);

    // create the image for transfer destination and sampling
    createImage(texWidth, texHeight, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, textureImage, textureImageMemory, device, physicalDevice);
    // transition the image from undefined to transfer destination to copy into
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, device, commandPool, graphicsQueue);
    // copy data from the staging buffer to the image
    copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight), device, commandPool, graphicsQueue);
    // transition the image to be used for sampling
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, device, commandPool, graphicsQueue);
    // destroy the buffer and free the memory
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

// create the descriptor set
void ModelRender::createDescriptorSets() {
    // need one descriptor set for each image in the swapchain
    std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
    // allocation info to allocate from the descriptor pool with computed size and data
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool;
    allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
    allocInfo.pSetLayouts = layouts.data();
    // resize to the number of images in the swapchain
    descriptorSets.resize(swapChainImages.size());
    // allocate the descriptor sets and check for success
    if (vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate descriptor sets");
    }
    // loop through the number of swapchain images to set each descriptor set
    for (size_t i = 0; i < swapChainImages.size(); ++i) {
        // link individual uniform buffer to the descriptor
        VkDescriptorBufferInfo bufferInfo{};
        bufferInfo.buffer = uniformBuffers[i];
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBufferObject);
        // link the texture to all the descriptors
        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = textureImageView;
        imageInfo.sampler = textureSampler;
        // 2 descriptor: texture and uniform buffer
        std::array<VkWriteDescriptorSet, 2> descriptorWrites{};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &bufferInfo;

        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo = &imageInfo;
        // save each descriptor set
        vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    }
}

// create the descriptor pool
void ModelRender::createDescriptorPool() {
    // the pool will hold one descriptor set for each swapchain image
    // with a descriptor for the uniform buffer and one for the texture
    std::array<VkDescriptorPoolSize, 2> poolSizes{};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size());

    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size());

    // create info for the descriptor pool with computed information
    VkDescriptorPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes = poolSizes.data();
    poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());

    // create the descriptor pool and check for success
    if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptor pool");
    }
}

// create the uniform buffers
void ModelRender::createUniformBuffers() {
    VkDeviceSize bufferSize = sizeof(UniformBufferObject);
    // need one uniform buffer per swapchain image
    uniformBuffers.resize(swapChainImages.size());
    uniformBuffersMemory.resize(swapChainImages.size());
    // create each uniform buffer
    for (size_t i = 0; i < swapChainImages.size(); ++i) {
        createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers[i], uniformBuffersMemory[i], device, physicalDevice);
    }
}

// create the layout for the descriptor sets
void ModelRender::createDescriptorSetLayout() {
    // layout binding for the uniform buffer to be used during vertex stage
    VkDescriptorSetLayoutBinding uboLayoutBinding{};
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    // layout binding for the texture sampler to be used in the fragment stage
    VkDescriptorSetLayoutBinding samplerLayoutBinding{};
    samplerLayoutBinding.binding = 1;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = nullptr;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding, samplerLayoutBinding };
    
    // combine the descriptor bindings in a create info for creation
    VkDescriptorSetLayoutCreateInfo layoutInfo{};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings = bindings.data();

    // create the layout and check for success
    if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptor set layout");
    }
}

// destroy the swap chain and all of its depending resources
void ModelRender::cleanUpSwapChain() {
    // destroy the depth resources
    vkDestroyImageView(device, depthImageView, nullptr);
    vkDestroyImage(device, depthImage, nullptr);
    vkFreeMemory(device, depthImageMemory, nullptr);

    // destroy all the uniform buffers and free the memory
    for (size_t i = 0; i < swapChainImages.size(); ++i) {
        vkDestroyBuffer(device, uniformBuffers[i], nullptr);
        vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
    }
    // destroy all the framebuffers
    for (auto frambuffer : swapChainFramebuffers) {
        vkDestroyFramebuffer(device, frambuffer, nullptr);
    }
    // free the command buffers
    vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t> (commandBuffers.size()), commandBuffers.data());
    // destroy the pipeline and its layout
    vkDestroyPipeline(device, graphicsPipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    // destroy the render pass
    vkDestroyRenderPass(device, renderPass, nullptr);
    // destroy the image views for the swap chain images
    for (auto imageView : swapChainImageViews) {
        vkDestroyImageView(device, imageView, nullptr);
    }
    // destroy the swapchain
    vkDestroySwapchainKHR(device, swapChain, nullptr);
    // destroy the descriptor pool
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
}

// recreate the swapchain after destruction
void ModelRender::recreateSwapChain() {
    int width = 0, height = 0;
    // get the current size of the window
    glfwGetFramebufferSize(window, &width, &height);
    // if minimized wait for un-minimizing
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(window, &width, &height);
        glfwWaitEvents();
    }
    // wait 
    vkDeviceWaitIdle(device);
    // clean up the swapchain before recreation
    cleanUpSwapChain();
    // create the swapchain and all of the depending resources
    createSwapChain();
    createImageViews();
    createRenderPass(renderPass);
    createGraphicsPipeline();
    createDepthResources();
    createFramebuffers();
    createUniformBuffers();
    createDescriptorPool();
    createDescriptorSets();
    createCommandBuffers();
}

// create the synchronization objects
void ModelRender::createSyncObjects() {
    // need semaphores and fences for each simultaneous frame
    imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    // need one fence for each swapchain image
    imagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);
    
    // semaphore and fence info
    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    // create the required semaphores and fences and check for success
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i])) {
            throw std::runtime_error("failed to create semaphores");
        }
    }
}

// update the command buffer for the current frame
void ModelRender::updateCommandBuffer(uint32_t currentFrame) {
    // reset the command buffer of the current frame
    vkResetCommandBuffer(commandBuffers[currentFrame], 0);

    // create info for the command buffer
	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;
	beginInfo.pInheritanceInfo = nullptr;

    // begin the command buffer and check for success
	if (vkBeginCommandBuffer(commandBuffers[currentFrame], &beginInfo) != VK_SUCCESS) {
		throw std::runtime_error("failed to begin recording command buffer");
	}

    // render pass info for the current frame
	VkRenderPassBeginInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = renderPass;
	renderPassInfo.framebuffer = swapChainFramebuffers[currentFrame];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = swapChainExtent;

    // clear values for the color and depth images
	std::array<VkClearValue, 2> clearValues = {};
	clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[1].depthStencil = { 1.0f, 0 };
	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();
    
    // begin the render pass
	vkCmdBeginRenderPass(commandBuffers[currentFrame], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    // bind the drawing pipeline
	vkCmdBindPipeline(commandBuffers[currentFrame], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
    // bind the vertex and index buffer with no offset
	VkBuffer vertexBuffers[] = { vertexBuffer };
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(commandBuffers[currentFrame], 0, 1, vertexBuffers, offsets);

	vkCmdBindIndexBuffer(commandBuffers[currentFrame], indexBuffer, 0, VK_INDEX_TYPE_UINT32);
    // add the descriptor sets
	vkCmdBindDescriptorSets(commandBuffers[currentFrame], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[currentFrame], 0, nullptr);
    // draw the model
	vkCmdDrawIndexed(commandBuffers[currentFrame], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);

    // mode the UI to the new frame
	imguiRender.newFrame();
    // update the vertex and index buffer values
	imguiRender.update();
    // draw the UI
	imguiRender.draw(commandBuffers[currentFrame]);

    // end the renderpass
	vkCmdEndRenderPass(commandBuffers[currentFrame]);
    // end the command buffer and check for success
	if (vkEndCommandBuffer(commandBuffers[currentFrame]) != VK_SUCCESS) {
		throw std::runtime_error("failed to record command buffer");
	}
}

// create the command buffers
void ModelRender::createCommandBuffers() {
    // need a command buffer for each swapchain image
    commandBuffers.resize(swapChainFramebuffers.size());

    // allocation info to allocate from command pool
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

    // allocate the command buffers and check for success
    if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate command buffers");
    }

    // loop through the command buffers
    for (size_t i = 0; i < commandBuffers.size(); ++i) {
        // begin info for each command buffer
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = 0;
        beginInfo.pInheritanceInfo = nullptr;

        // begin the command buffer and check for result
        if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS) {
            throw std::runtime_error("failed to begin recording command buffer");
        }

		// begin info for the render pass creation
        VkRenderPassBeginInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = renderPass;
        renderPassInfo.framebuffer = swapChainFramebuffers[i];
        renderPassInfo.renderArea.offset = { 0, 0 };
        renderPassInfo.renderArea.extent = swapChainExtent;
        // clear values for the color and depth
        std::array<VkClearValue, 2> clearValues = {};
        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f, 0 };
        renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        renderPassInfo.pClearValues = clearValues.data();
        // begin the render pass
        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        // bind the drawing pipeline
        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
        // bind the vertex and index buffers
        VkBuffer vertexBuffers[] = { vertexBuffer };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);

        vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT32);
        // bind the descriptor set
        vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[i], 0, nullptr);
        // draw the model
        vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
        // move the UI to a new frame
		imguiRender.newFrame();
        // update the vertex and index buffers
		imguiRender.update();
        // draw the UI
        imguiRender.draw(commandBuffers[i]);

        // end the renderpass
        vkCmdEndRenderPass(commandBuffers[i]);
        // end the command buffer and check for success
        if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
            throw std::runtime_error("failed to record command buffer");
        }
    }
}

// create the command pool
void ModelRender::createCommandPool() {
    // find the available queue families
    QueueFamilyIndices queueFamiliyIndices = findQueueFamilies(
        physicalDevice, surface);
    // create the info using the graphics queue
    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamiliyIndices.graphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    // try to create the command pool and check for result
    if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
        throw std::runtime_error("failed to create command pool");
    }
}

// create the frame buffers
void ModelRender::createFramebuffers() {
    // need one framebuffer for each swap chain image
    swapChainFramebuffers.resize(swapChainImageViews.size());

    // loop through the swapchain images
    for (size_t i = 0; i < swapChainImageViews.size(); ++i) {
        // need an image view and a depth view for each framebuffer
        std::array<VkImageView, 2> attachments = { swapChainImageViews[i], depthImageView };
        // creation info to link the renderpass, attachments and sizes
        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        framebufferInfo.pAttachments = attachments.data();
        framebufferInfo.width = swapChainExtent.width;
        framebufferInfo.height = swapChainExtent.height;
        framebufferInfo.layers = 1;

        // create the framebuffer and check for success
        if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS) {
            throw std::runtime_error("failed to create framebuffer");
        }
    }
}

// create the renderpass
void ModelRender::createRenderPass(VkRenderPass& renderPass) {
    // set up the color attachment of the renderpass
    // from undefined to present, no mip-map, clearing on loading
    // and storing on save
    VkAttachmentDescription colorAttachment{};
    colorAttachment.format = swapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    // create a reference for the color attachment
    VkAttachmentReference colorAttachmentRef{};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    // setup the depth attachment, clear on load, don't care for save,
    // starting from undefined layout to depth optimal
    VkAttachmentDescription depthAttachment{};
    depthAttachment.format = findDepthFormat();
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    // create a reference for the depth attachment
    VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    // create a subpass for the the renderpass that draws the model
    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
    // two dependencies for drawing
    std::array<VkSubpassDependency, 2> dependencies;
    // wait for everything to end before reading/writing to the color attachment 
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
    // wait for the color attachment read/write to finish before ending the pass
	dependencies[1].srcSubpass = 0;
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };
    // combine the information in the render creation info
    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = static_cast<uint32_t> (dependencies.size());
    renderPassInfo.pDependencies = dependencies.data();
    // create the render pass and check for success
    if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
        throw std::runtime_error("failed to create render pass");
    }
}

// create the graphics pipeline
void ModelRender::createGraphicsPipeline() {
    // load the shaders for the model and create the shader modules
    auto vertShaderCode = readFile("shaders/vert.spv");
    auto fragShaderCode = readFile("shaders/frag.spv");

    VkShaderModule vertShaderModule = createShaderModule(vertShaderCode, device);
    VkShaderModule fragShaderModule = createShaderModule(fragShaderCode, device);

    // create info for the vertex stage, linking to the vertex shader
    // module and having main as the starting point
    VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
    vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";
	// create info for the fragment stage, linking to the fragment shader
	// module and having main as the starting point
    VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
    fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

    // get the binding and attributes from the vertex struct
    auto bindingDescription = Vertex::getBindingDescription();
    auto attributeDescription = Vertex::getAttributeDescriptions();

    // setup the input for the vertex stage, using the vertex struct
    // attributes and bindings
    VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescription.size());
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescription.data();

    // use triangle list as input primitives
    VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    // create the viewport with given size
    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float)swapChainExtent.width;
    viewport.height = (float)swapChainExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    // create the scrissor 
    VkRect2D scrissor{};
    scrissor.offset = { 0, 0 };
    scrissor.extent = swapChainExtent;

    // combine the viewport and the scissor in the viewport state
    VkPipelineViewportStateCreateInfo viewportState{};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scrissor;

    // setup the rasterizer stage parameters: no depth clamping,
    // no discarding, polygons are filled, culling the back bit,
    // the front face is counter cw and no depth bias is used
    VkPipelineRasterizationStateCreateInfo rasterizer{};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;

    // multisampling is not used 
    VkPipelineMultisampleStateCreateInfo multisampling{};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    // setup the color blending attachment - no blending
    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;

    // setup blending - no blending
    VkPipelineColorBlendStateCreateInfo colorBlending{};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;

    // setup depth stencil - with depth test
    VkPipelineDepthStencilStateCreateInfo depthStencil{};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.stencilTestEnable = VK_FALSE;

    // setup the pipeline layout
    VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;

    // create the pipeline layout
    if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create pipeline layout");
    }

    // pipeline creation info, combining all the structures created before
    VkGraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = renderPass;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex = -1;
    pipelineInfo.pDepthStencilState = &depthStencil;

    // create the graphics pipeline and check for success
    if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
        throw std::runtime_error("failed to create graphics pipeline");
    }

    // destroy the shader modules
    vkDestroyShaderModule(device, fragShaderModule, nullptr);
    vkDestroyShaderModule(device, vertShaderModule, nullptr);
}

// create the image views for the swapchain images
void ModelRender::createImageViews() {
    // need one image view per image in the swapchain
    swapChainImageViews.resize(swapChainImages.size());
    
    // loop through the images and create one image view for each
    for (size_t i = 0; i < swapChainImages.size(); ++i) {
        swapChainImageViews[i] = createImageView(device, swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
    }
}

// create the swapchain
void ModelRender::createSwapChain() {
    // query what type of support is there for the used physical device
    // and khr surface
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice, surface);
    // choose format, present mode and extent from the available ones
    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
    VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);
    // set the image count as the minimum supported plus 1
    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;

    // if the maximum image count is a positive number but the current
    // computed image count is larger than the maximum available,
    // use the maximum available
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    // swapchain creation info that uses the fields computed before
    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    // find the available queue families
    QueueFamilyIndices indices = findQueueFamilies(physicalDevice, surface);
    uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

    // if the present queue and the graphics queue are different, set
    // the sharing mode as concurrent (even if maybe slower) so we
    // don t need to deal with changing ownership in exclusive mode
    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else {
        // else set as exclusive because it's only one queue anyway
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = nullptr;
    }

    // setup the pre-transform from the surface and the present mode
    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    // no previous information for the swapchain
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    // create the swapchain and check for success
    if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
        throw std::runtime_error("failed to create swap chain");
    }
    // get the number of swapchain images actually created
    vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
    // get the images created
    swapChainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
    // save the format and the extent
    swapChainImageFormat = surfaceFormat.format;
    swapChainExtent = extent;
}

// create the window surface
void ModelRender::createSurface() {
    // try to create the window surface and check for success
    if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface");
    }
}

// create a logical device
void ModelRender::createLogicalDevice() {
    // query the available queue families
    QueueFamilyIndices indices = findQueueFamilies(physicalDevice, surface);
    // create information for the queues
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    // set to keep unique indices of the presenting and graphics queues
    std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

    float queuePriority = 1.0f;
    // loop through the unique queue indices
    for (uint32_t queueFamily : uniqueQueueFamilies) {
        // creation information with fixed priority and queue family
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    // want sampler anisotropy for the sampler as device feature
    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    //  creation info for the device
    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    // use the computed queue information
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    // use the required device features
    createInfo.pEnabledFeatures = &deviceFeatures;
    // use the required device extensions
    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();
    // if validation is enabled, require its layer
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }
    else {
        createInfo.enabledLayerCount = 0;
    }
    // create the device and check for success
    if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
        throw std::runtime_error("failed to create logical device");
    }
    // get a handle for each of the queues
    vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
    vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
}

// pick an adequate physical device
void ModelRender::pickPhysicalDevice() {
    // get the number of physical devices available
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
    // if no vulkan compatible device, it's an issue
    if (deviceCount == 0) {
        throw std::runtime_error("failed to find GPUs with Vulkan support");
    }
    // get the physical devices
    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
    // loop through the available devices and take the first one that is suitable
    for (const auto& device : devices) {
        if (isDeviceSuitable(device, surface)) {
            physicalDevice = device;
            break;
        }
    }
    // a better approach would be to rate the available devices and
    // pick the best one, but first one also suffices
    /*
    std::multimap<int, VkPhysicalDevice>candidates;

    for (const auto& device : devices) {
        int score = rateDeviceSuitability(device);
        candidates.insert(std::make_pair(score, device));
    }

    if (candidates.rbegin()->first > 0) {
        physicalDevice = candidates.rbegin()->second;
    }
    else {
        throw std::runtime_error("failed to find a suitable GPU");
    }
    */
    // if no device is suitable, we have an issue
    if (physicalDevice == VK_NULL_HANDLE) {
        throw std::runtime_error("failed to find a suitable GPU");
    }
}

// choose swap extent
VkExtent2D ModelRender::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
    // if there is a value set, UINTMAX_32 is a special value to
    // represent not set, return the value
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    }
    else {
        // else need to setup some extent values
        int width, height;
        // get the size of the framebuffer
        glfwGetFramebufferSize(window, &width, &height);
        // set them as the actual extent
        VkExtent2D actualExtent = {
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height)
        };
        // bound the width and height between the maximum and minimum available sizes
        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

// create a vulkan instane
void ModelRender::createInstance() {
    // if we need validation, but they are not supported, there is an issue
    if (enableValidationLayers && !checkValidationLayerSupport()) {
        throw std::runtime_error("validation layers requested, but not available!");
    }

    // application info setup: with name and vulkan versions
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 2, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 2, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    // get the required extensions
    auto extensions = getRequiredExtensions();

    // set them as part of the creation info
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();
    createInfo.enabledLayerCount = 0;

    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    // if validation is enabled
    if (enableValidationLayers) {
        // need to add the validation layer
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
        // and populate the debugging messenger creation info
        // and add it to the create structure
        populateDebugMessengerCreateInfo(debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugCreateInfo;
    }
    else {
        createInfo.enabledLayerCount = 0;
        createInfo.pNext = nullptr;
    }
    // create the instance  and check for success
    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
        throw std::runtime_error("failed to create instance!");
    }

    /* print all the available extensions
    uint32_t extensionCount = 0;

    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);

    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

    std::cout << "Available extensions:\n";

    for (const auto& extension : extensions) {
        std::cout << '\t' << extension.extensionName << std::endl;
    }
    */
}

// populate the debug messenger create info
void ModelRender::populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
  
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    // want warnings and errors from the validation layer
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    // want general, validation and performance messages
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    // link the debug function
    createInfo.pfnUserCallback = debugCallback;
    createInfo.pUserData = nullptr;
}

// setup the debug messenger
void ModelRender::setupDebugMessenger() {
    // if validation is not enabled, need no debug messenger
    if (!enableValidationLayers) {
        return;
    }

    // creation information for the messenger
    VkDebugUtilsMessengerCreateInfoEXT createInfo;
    populateDebugMessengerCreateInfo(createInfo);
    // create the debug messenger and check for success
    if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
        throw std::runtime_error("failed to set up debug messenger!");
    }
}

// main loop of the program
void ModelRender::mainLoop() {
    // while the window is open
    while (!glfwWindowShouldClose(window)) {
        // check for new events
        glfwPollEvents();
        // draw the frame
        drawFrame();
    }
    // wait for device to become idle - all operations to have finished
    // before exiting the function
    vkDeviceWaitIdle(device);
}

// draw a frame
void ModelRender::drawFrame() {
    // wait for the fence of the current frame
    vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);
    // image index to use for acquired new image
    uint32_t imageIndex;
    // acquire new image from the swapchain, use the current frame's semaphore 
    VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

    // if the image is out of date, need to recreate the swapchain
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain();
        return;
    }
    // else if the result is an error, but not suboptimal
    else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("failed to acquire swap chain image");
    }
    // if the fence for the acquired image is not null, wait for it
    if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(device, 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    }
    // assign the in flight fence of the current frame to the acquired
    // image's fence
    imagesInFlight[imageIndex] = inFlightFences[currentFrame];
    // update the command buffer of the current image
    updateCommandBuffer(imageIndex );
    // update the uniform buffers of the current image
    updateUniformBuffer(imageIndex);
    // submit information for the queue
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    // semaphores we need to wait for i.e. the current frame's semaphore
    VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
    // wait stages for the semaphore 
    VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
    // this will signal the given semaphore on end
    VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;
    // reset the current frame's fence
    vkResetFences(device, 1, &inFlightFences[currentFrame]);
    // submit to graphics queue and check for success
    if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS) {
        throw std::runtime_error("failed to submit draw command buffer");
    }
    // present information to wait for the semaphore
    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    // swapchain information
    VkSwapchainKHR swapChains[] = { swapChain };
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;

    // present to the present queue
    result = vkQueuePresentKHR(presentQueue, &presentInfo);
    // if the result is out of date or suboptimal, recreate the swapchain
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || frameBufferResized) {
        frameBufferResized = false;
        recreateSwapChain();
    }
    else if (result != VK_SUCCESS) {
        throw std::runtime_error("failed to present swap chain image");
    }
    // advance the current frame, wrap around after the max frames in flight
    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

// update the uniform buffer of the current image
void ModelRender::updateUniformBuffer(uint32_t currentImage) {
    // create the uniform buffer
    UniformBufferObject ubo{};
    // set the translation from the UI settings
    ubo.model = glm::translate(glm::mat4(1.0f), imguiRender.uiSettings.modelPosition);
    // set the rotation from the UI settings on all three axes
    ubo.model = glm::rotate(ubo.model, glm::radians(imguiRender.uiSettings.modelRotation.x), glm::vec3(1.0f, 0.0, 0.0f));
    ubo.model = glm::rotate(ubo.model, glm::radians(imguiRender.uiSettings.modelRotation.y), glm::vec3(0.0f, 1.0, 0.0f));
    ubo.model = glm::rotate(ubo.model, glm::radians(imguiRender.uiSettings.modelRotation.z), glm::vec3(0.0f, 0.0, 1.0f));
    // set the view 
    ubo.view = glm::lookAt(glm::vec3(0.0f, 1.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    // set the projection as perspective
    ubo.proj = glm::perspective(glm::radians(45.0f), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 10.0f);
    // flip the y axis
    ubo.proj[1][1] *= -1;
    // normal matrix for the normals
    ubo.normal = inverse(transpose(ubo.view * ubo.model));
    // set the light position from the UI settings
    ubo.lightPos = glm::vec4(imguiRender.uiSettings.lightPosition, 0.0f);
    // set the states of rendering
    ubo.states = glm::vec4((float)imguiRender.uiSettings.displayModels, (float)imguiRender.uiSettings.displayTexture, (float)imguiRender.uiSettings.displayPhong, 0.0f);
    // set the material from the mtl files
    ubo.ambientColor = defaultUBO.ambientColor;
    ubo.diffuseColor = defaultUBO.diffuseColor;
    ubo.specularColor = defaultUBO.specularColor;
    // data to use for copying to the uniform buffer, to map and unmap
    void* data;
    vkMapMemory(device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
    memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(device, uniformBuffersMemory[currentImage]);
}

// cleanup the resources
void ModelRender::cleanup() {
    // free the UI resources
    imguiRender.freeResources();
    // free the swapchain resources
    cleanUpSwapChain();
    // destroy the sampler, image view and image, and free memory of texture
    vkDestroySampler(device, textureSampler, nullptr);
    vkDestroyImageView(device, textureImageView, nullptr);
    vkDestroyImage(device, textureImage, nullptr);
    vkFreeMemory(device, textureImageMemory, nullptr);

    // destroy the descriptor set layout
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
    // destroy the vertex and index buffers and free their memory
    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);

    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr);
    // destroy all the semaphores and fences
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
        vkDestroyFence(device, inFlightFences[i], nullptr);
    }
    // destroy the command pool
    vkDestroyCommandPool(device, commandPool, nullptr);
    // destroy the logical device
    vkDestroyDevice(device, nullptr);
    // destroy the debug messenger if the validation layers were enabled
    if (enableValidationLayers) {
        DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
    }
    // destroy the khr surface
    vkDestroySurfaceKHR(instance, surface, nullptr);
    // destroy the vk instance
    vkDestroyInstance(instance, nullptr);
    // destroy the window
    glfwDestroyWindow(window);
    // end the glfw context
    glfwTerminate();
}
