#pragma once
#include"vkUtils.h"
#include "imGuiClass.h"

// initial window sizes
const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;
// frames in flight
const int MAX_FRAMES_IN_FLIGHT = 2;

// class to render the duck
class ModelRender {
public:
    // main running function
    void run() {
        // initialises the glfw window
        initWindow();
        // initialises all the required vulkan resources
        initVulkan();
        // renders frames as long as it is needed
        mainLoop();
        // frees the used resources at the end
        cleanup();
    }
private:
    // UI renderer
    ImGuiClass imguiRender;
   
    GLFWwindow* window;
    VkDebugUtilsMessengerEXT debugMessenger;
   
    VkInstance instance;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkDevice device;
    // queues for drawing geometry and for presenting to screen
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    // surface to present to
    VkSurfaceKHR surface;

    VkSwapchainKHR swapChain;
    std::vector<VkImage> swapChainImages;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    std::vector<VkImageView> swapChainImageViews;
    // need only one render pass
    VkRenderPass renderPass;
    
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;
    std::vector<VkFramebuffer> swapChainFramebuffers;
    
    VkCommandPool commandPool;
    std::vector<VkCommandBuffer> commandBuffers;
    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphores;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;
    size_t currentFrame = 0;
    
    bool frameBufferResized = false;
    
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;
    
    std::vector<VkBuffer> uniformBuffers;
    std::vector<VkDeviceMemory> uniformBuffersMemory;
    
    VkDescriptorPool descriptorPool;
    std::vector<VkDescriptorSet> descriptorSets;
    
    VkImage textureImage;
    VkDeviceMemory textureImageMemory;
    VkImageView textureImageView;
    VkSampler textureSampler;
    
    VkImage depthImage;
    VkDeviceMemory depthImageMemory;
    VkImageView depthImageView;
    // structure to save the initial material configuration from mtl
    UniformBufferObject defaultUBO;
    // vertices and indices for the model
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;

    // initialize the glfw window
    void initWindow();
    // callback for resizing
    static void frameBufferResizeCallback(GLFWwindow* window, int width, int height);
    // initialize the vulkan resources
    void initVulkan();

    // resource creation
    void createInstance();
    void createLogicalDevice();
    void createSurface();

    void createTextureImage();
    void createTextureImageView();
    void createDescriptorSets();
    void createDepthResources();

    void createSyncObjects();

    void createDescriptorPool();
    void createUniformBuffers();
    void createDescriptorSetLayout();

    void createCommandBuffers();
    void createCommandPool();
    void createFramebuffers();
    void createRenderPass(VkRenderPass& renderPass);
    void createGraphicsPipeline();

    void createSwapChain();
    void createImageViews();

    // debug function to print the errors from the validation layer
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData) {
        std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

        return VK_FALSE;
    };
    // debug messenger creation
    void setupDebugMessenger();
    void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

    // update resources
    void updateUniformBuffer(uint32_t currentImage);
    void updateCommandBuffer(uint32_t currentFrame);
    void recreateSwapChain();

    // drawing functions
    void mainLoop();
    void drawFrame();
    // cleanup
    void cleanup();
    void cleanUpSwapChain();
    
    // helper functions
    void pickPhysicalDevice();
    VkFormat findDepthFormat();
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

};
