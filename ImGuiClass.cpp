#include "ImGuiClass.h"

// Constructor - set initial states
ImGuiClass::ImGuiClass() {
	// initialise the imgui context
	ImGui::CreateContext();
	// set the style as dark
	ImGui::StyleColorsDark();
	// set dimensions of the font
	ImGuiIO& io = ImGui::GetIO();
	io.FontGlobalScale = scale;
	// do not save/load last UI positions from file
	io.IniFilename = nullptr;
	// set the initial model rotation
	// the others are not implicitly set, the struct constructor
	// takes care of that
	uiSettings.modelRotation = glm::vec3(-90.0f, 0.0f, 0.0f);
}

// Destructor - does nothing
ImGuiClass::~ImGuiClass() {}

// Prepare the resources for drawing
void ImGuiClass::prepareResources() {
	// get IO from ImGui
	ImGuiIO& io = ImGui::GetIO();
	// create font texture
	io.Fonts->GetTexDataAsRGBA32(&fontData, &texWidth, &texHeight);
	// compute the total size
	VkDeviceSize uploadSize = texWidth * texHeight * 4 * sizeof(char);

	// create target image for copy: it is a 2d image,
	// with rgba of 8 each, given width and height, no mip-maping
	// it will be sampled and used for transfer; we need
	// this to create the image for the font
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	imageInfo.extent.width = texWidth;
	imageInfo.extent.height = texHeight;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	// check if image creation was successful
	CheckResult(vkCreateImage(*device, &imageInfo, nullptr, &fontImage));
	// get the memory requirements for the font image created
	VkMemoryRequirements memReqs;
	vkGetImageMemoryRequirements(*device, fontImage, &memReqs);
	
	// allocate the memory for the image, using the computed memory
	// requirements and a helper function for the memory type required
	VkMemoryAllocateInfo memAllocInfo{};
	memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocInfo.allocationSize = memReqs.size;
	memAllocInfo.memoryTypeIndex = findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *physicalDevice);
	// check to see if memory allocation was successful
	CheckResult(vkAllocateMemory(*device, &memAllocInfo, nullptr, &fontMemory));
	// check if the binding of the image and memory was successful
	CheckResult(vkBindImageMemory(*device, fontImage, fontMemory, 0));

	// create the image view for the font image
	VkImageViewCreateInfo viewInfo{};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = fontImage;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.layerCount = 1;
	// check if creation of image view was successful
	CheckResult(vkCreateImageView(*device, &viewInfo, nullptr, &fontView));

	// staging buffers for font data upload
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemoryBuffer;

	// create a buffer for staging the font data
	createBuffer(
		uploadSize,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer,
		stagingMemoryBuffer,
		*device,
		*physicalDevice);

	// map the data from device to host into data and unmap after copying
	void* data;
	vkMapMemory(*device, stagingMemoryBuffer, 0, uploadSize, 0, &data);
	memcpy(data, fontData, uploadSize);
	vkUnmapMemory(*device, stagingMemoryBuffer);

	// change the image layout from undefined to destination-optimal
	// because we need it to be able to copy from the buffer to image
	transitionImageLayout(fontImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, *device, commandPool, queue);
	// copy the buffer to image
	copyBufferToImage(stagingBuffer, fontImage, texWidth, texHeight, *device, commandPool, queue);
	// transition the font image from previous layout to shader read only,
	// because we need it to be used in shaders for drawing
	transitionImageLayout(fontImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, *device, commandPool, queue);


	// font texture sampler, with linear filtering, clamping to edge
	// and border color of white
	VkSamplerCreateInfo samplerInfo{};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
	// check if the sampler was successfully created
	CheckResult(vkCreateSampler(*device, &samplerInfo, nullptr, &sampler));

	// descriptor pool size for one descriptor pool
	VkDescriptorPoolSize descriptorPoolSize{};
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	descriptorPoolSize.descriptorCount = 1;

	std::vector<VkDescriptorPoolSize> poolSizes = {
		descriptorPoolSize	
	};

	// descriptor pool with at most 2 sets
	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
	descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	descriptorPoolCreateInfo.pPoolSizes = poolSizes.data();
	descriptorPoolCreateInfo.maxSets = 2;
	// check if the descriptor pool has been successfully created
	CheckResult(vkCreateDescriptorPool(*device, &descriptorPoolCreateInfo, nullptr, &descriptorPool));

	// descriptor set layout binding - descriptor to be used in the 
	// fragment stage
	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding{};
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorCount = 1;

	std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings = {
		descriptorSetLayoutBinding
	};

	// descriptor set layout
	VkDescriptorSetLayoutCreateInfo descriptorLayout{};
	descriptorLayout.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorLayout.pBindings = setLayoutBindings.data();
	descriptorLayout.bindingCount = static_cast<uint32_t>(setLayoutBindings.size());
	// check if the descriptor set layout has been correctly set
	CheckResult(vkCreateDescriptorSetLayout(*device, &descriptorLayout, nullptr, &descriptorSetLayout));

	// descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.pSetLayouts = &descriptorSetLayout;
	allocInfo.descriptorSetCount = 1;
	// check if the descriptor set has been correctly set
	CheckResult(vkAllocateDescriptorSets(*device, &allocInfo, &descriptorSet));

	// descriptor for the font image, with its image and sampler to be read
	VkDescriptorImageInfo fontDescriptor{};
	fontDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	fontDescriptor.imageView = fontView;
	fontDescriptor.sampler = sampler;
	
	// write structure for the font descriptor set
	VkWriteDescriptorSet writeDescriptorSet{};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.dstSet = descriptorSet;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeDescriptorSet.dstBinding = 0;
	writeDescriptorSet.pImageInfo = &fontDescriptor;
	writeDescriptorSet.descriptorCount = 1;

	std::vector<VkWriteDescriptorSet> writeDescriptorSets = {
		writeDescriptorSet
	};

	// update the descriptor sets using the write structure
	vkUpdateDescriptorSets(*device, static_cast<uint32_t>(writeDescriptorSets.size()), writeDescriptorSets.data(), 0, nullptr);

	// destroy the staging buffer and free the memory used for it
	vkDestroyBuffer(*device, stagingBuffer, nullptr);
	vkFreeMemory(*device, stagingMemoryBuffer, nullptr);
}

// Prepare the pipeline for drawing
void ImGuiClass::preparePipeline(const VkPipelineCache pipelineCache, const VkRenderPass& renderPass)
{
	// push constants for UI rendering parameters
	VkPushConstantRange pushConstantRange{};
	pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	pushConstantRange.offset = 0;
	pushConstantRange.size = sizeof(PushConstBlock);
	
	// pipeline layout using the push constants
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
	pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;
	// check if the layout was correctly computed
	CheckResult(vkCreatePipelineLayout(*device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	// read the UI shaders & create the shader modules
	auto vertShaderCode = readFile("shaders/uivert.spv");
	auto fragShaderCode = readFile("shaders/uifrag.spv");

	VkShaderModule vertShaderModule = createShaderModule(vertShaderCode, *device);
	VkShaderModule fragShaderModule = createShaderModule(fragShaderCode, *device);

	// create the vertex and fragment stages info, linking the shader
	// modules and starting point as main
	VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";

	shaders = { vertShaderStageInfo, fragShaderStageInfo };


	// setup graphics pipeline for UI rendering
	
	// setup the input assembly state to accept a triangle list
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyState{};
	inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyState.flags = 0;
	inputAssemblyState.primitiveRestartEnable = VK_FALSE;
	
	// setup the rasterisation stage with fill mode, no culling
	VkPipelineRasterizationStateCreateInfo rasterizationState{};
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizationState.cullMode = VK_CULL_MODE_NONE;
	rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizationState.flags = 0;
	rasterizationState.depthClampEnable = VK_FALSE;
	rasterizationState.lineWidth = 1.0f;
	
	// enable blending for the alpha channel for the font
	VkPipelineColorBlendAttachmentState blendAttachmentState{};
	blendAttachmentState.blendEnable = VK_TRUE;
	blendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	blendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	blendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	blendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	blendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	blendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	blendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlendState{};
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendState.attachmentCount = 1;
	colorBlendState.pAttachments = &blendAttachmentState;

	// create the depth stencil that is not using depth test
	VkPipelineDepthStencilStateCreateInfo depthStencilState{};
	depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilState.depthTestEnable = VK_FALSE;
	depthStencilState.depthWriteEnable = VK_FALSE;
	depthStencilState.depthCompareOp = VK_COMPARE_OP_ALWAYS;
	depthStencilState.back.compareOp = VK_COMPARE_OP_ALWAYS;
	
	// create the viewport with one viewport and one scissor
	VkPipelineViewportStateCreateInfo viewportState{};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;
	viewportState.flags = 0;
	
	// create the multisample state for the pipeline
	VkPipelineMultisampleStateCreateInfo multisampleState{};
	multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleState.rasterizationSamples = rasterizationSamples;
	multisampleState.flags = 0;
	
	// dynamic states because the viewport and scissor change
	// for each element and they need to be dynamic
	std::vector<VkDynamicState> dynamicStateEnables = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};
	VkPipelineDynamicStateCreateInfo dynamicState{};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pDynamicStates = dynamicStateEnables.data();
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());
	dynamicState.flags = 0;

	// finally create the pipeline info with all the stages created before
	// and provided renderpass
	VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.layout = pipelineLayout;
	pipelineCreateInfo.renderPass = renderPass;
	pipelineCreateInfo.flags = 0;
	pipelineCreateInfo.basePipelineIndex = -1;
	pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pMultisampleState = &multisampleState;
	pipelineCreateInfo.pViewportState = &viewportState;
	pipelineCreateInfo.pDepthStencilState = &depthStencilState;
	pipelineCreateInfo.pDynamicState = &dynamicState;
	pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaders.size());
	pipelineCreateInfo.pStages = shaders.data();
	pipelineCreateInfo.subpass = subpass;

	// input descriptor with the size of drawn vertices
	VkVertexInputBindingDescription vInputBindDescription{};
	vInputBindDescription.binding = 0;
	vInputBindDescription.stride = sizeof(ImDrawVert);
	vInputBindDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	std::vector<VkVertexInputBindingDescription> vertexInputBindings = {
		vInputBindDescription
	};

	// vertex attributes based on ImGui vertex definition
	VkVertexInputAttributeDescription vInputAttribDescription{};
	vInputAttribDescription.location = 0;
	vInputAttribDescription.binding = 0;
	vInputAttribDescription.format = VK_FORMAT_R32G32_SFLOAT;
	vInputAttribDescription.offset = offsetof(ImDrawVert, pos);

	VkVertexInputAttributeDescription uvInputAttribDescription{};
	uvInputAttribDescription.location = 1;
	uvInputAttribDescription.binding = 0;
	uvInputAttribDescription.format = VK_FORMAT_R32G32_SFLOAT;
	uvInputAttribDescription.offset = offsetof(ImDrawVert, uv);

	VkVertexInputAttributeDescription colorInputAttribDescription{};
	colorInputAttribDescription.location = 2;
	colorInputAttribDescription.binding = 0;
	colorInputAttribDescription.format = VK_FORMAT_R8G8B8A8_UNORM;
	colorInputAttribDescription.offset = offsetof(ImDrawVert, col);

	std::vector<VkVertexInputAttributeDescription> vertexInputAttributes = {
		vInputAttribDescription, uvInputAttribDescription, colorInputAttribDescription
	};

	// vertex input states with ImGui vertex structure
	VkPipelineVertexInputStateCreateInfo vertexInputState{};
	vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputState.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexInputBindings.size());
	vertexInputState.pVertexBindingDescriptions = vertexInputBindings.data();
	vertexInputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexInputAttributes.size());
	vertexInputState.pVertexAttributeDescriptions = vertexInputAttributes.data();
	// add the vertex input state to the pipeline info
	pipelineCreateInfo.pVertexInputState = &vertexInputState;
	// check if the pipeline creation is successful
	CheckResult(vkCreateGraphicsPipelines(*device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &pipeline));
	
	// destroy the shader modules
	vkDestroyShaderModule(*device, vertShaderModule, nullptr);
	vkDestroyShaderModule(*device, fragShaderModule, nullptr);
}

//Update vertex and index buffer containing the imGui elements when required 
bool ImGuiClass::update()
{
	// get the drawing data for ImGui
	ImDrawData* imDrawData = ImGui::GetDrawData();
	bool updateCmdBuffers = false;
	
	// if no data to draw, return
	if (!imDrawData) { return false; };

	// get the vertex and index sizes, alignment is done inside buffer creation
	VkDeviceSize vertexBufferSize = imDrawData->TotalVtxCount * sizeof(ImDrawVert);
	VkDeviceSize indexBufferSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);

	// update buffers only if vertex or index count has been changed compared to current buffer size
	if ((vertexBufferSize == 0) || (indexBufferSize == 0)) {
		return false;
	}

	// vertex buffer recreation if there is no buffer or the number of vertices changed
	if ((vertexBuffer == VK_NULL_HANDLE) || (vertexCount != imDrawData->TotalVtxCount)) {
		// if there was a buffer before
		if (vertexBuffer != VK_NULL_HANDLE) {
			// unmap the memory, destroy the buffer and free the memory
			vkUnmapMemory(*device, vertexDeviceMemory);
			vkFreeMemory(*device, vertexDeviceMemory, nullptr);
			vkDestroyBuffer(*device, vertexBuffer, nullptr);
		}
		
		// now recreate the buffer with given size
		createBuffer(vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, vertexBuffer, vertexDeviceMemory, *device, *physicalDevice);

		vertexCount = imDrawData->TotalVtxCount;
		// map the memory on host
		vkMapMemory(*device, vertexDeviceMemory, 0, VK_WHOLE_SIZE, 0, &vertexData);
		// need to update the command buffer
		updateCmdBuffers = true;
	}

	// index buffer recreation
	VkDeviceSize indexSize = imDrawData->TotalIdxCount * sizeof(ImDrawIdx);
	// if the index buffer does not exist or size has been changed
	if ((indexBuffer == VK_NULL_HANDLE) || (indexCount != imDrawData->TotalIdxCount)) {
		// if there was a buffer before
		if (indexBuffer != VK_NULL_HANDLE) {
			// unmap its memory, destroy the buffer and free the memory
			vkUnmapMemory(*device, indexDeviceMemory);
			vkFreeMemory(*device, indexDeviceMemory, nullptr);
			vkDestroyBuffer(*device, indexBuffer, nullptr);
		}

		// create buffer with given size
		createBuffer(indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, indexBuffer, indexDeviceMemory, *device, *physicalDevice);

		indexCount = imDrawData->TotalIdxCount;
		// map the memory on host
		vkMapMemory(*device, indexDeviceMemory, 0, VK_WHOLE_SIZE, 0, &indexData);
		// need to update the command buffers
		updateCmdBuffers = true;
	}

	// upload data
	ImDrawVert* vtxDst = (ImDrawVert*)vertexData;
	ImDrawIdx* idxDst = (ImDrawIdx*)indexData;

	// loop through the command lists of the UI
	for (int n = 0; n < imDrawData->CmdListsCount; n++) {
		// get the current UI element's indices and vertices 
		const ImDrawList* cmd_list = imDrawData->CmdLists[n];
		memcpy(vtxDst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
		memcpy(idxDst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		// add the data to the global data
		vtxDst += cmd_list->VtxBuffer.Size;
		idxDst += cmd_list->IdxBuffer.Size;
	}

	// flush vertices and indices to make writes visible to GPU

	VkMappedMemoryRange mappedRange = {};
	mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedRange.memory = vertexDeviceMemory;
	mappedRange.offset = 0;
	mappedRange.size = VK_WHOLE_SIZE;
	vkFlushMappedMemoryRanges(*device, 1, &mappedRange);

	VkMappedMemoryRange indexMappedRange = {};
	indexMappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	indexMappedRange.memory = indexDeviceMemory;
	indexMappedRange.offset = 0;
	indexMappedRange.size = VK_WHOLE_SIZE;
	vkFlushMappedMemoryRanges(*device, 1, &mappedRange);

	// return command buffers need updating
	return updateCmdBuffers;
}

// Draw the current data for a frame in a given command buffer
void ImGuiClass::draw(const VkCommandBuffer commandBuffer)
{
	// get the draw data from ImGio
	ImDrawData* imDrawData = ImGui::GetDrawData();
	int32_t vertexOffset = 0;
	int32_t indexOffset = 0;

	// if nothing to draw, return
	if ((!imDrawData) || (imDrawData->CmdListsCount == 0)) {
		return;
	}
	// get the IO from ImGui
	ImGuiIO& io = ImGui::GetIO();
	// if the application is minimised( or any dimension is 0)
	if (io.DisplaySize.x == 0.0f || io.DisplaySize.y == 0.0f) {
		return;
	}

	// bind the UI pipeline and descriptor sets
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);
	
	// set the viewport dimensions and set the viewport
	VkViewport viewport{};
	viewport.width = io.DisplaySize.x;
	viewport.height = io.DisplaySize.y;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

	// set the push constants for scale and translation and push them
	pushConstBlock.scale = glm::vec2(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y);
	pushConstBlock.translate = glm::vec2(-1.0f);
	vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(PushConstBlock), &pushConstBlock);

	// bind the vertex and index buffers 
	VkDeviceSize offsets[1] = { 0 };
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffer, offsets);
	vkCmdBindIndexBuffer(commandBuffer, indexBuffer, 0, sizeof(ImDrawIdx) == 2 ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);

	// for each command element in ImGui
	for (int32_t i = 0; i < imDrawData->CmdListsCount; i++)
	{
		// get the current command and loop through the associated buffer
		const ImDrawList* cmd_list = imDrawData->CmdLists[i];
		for (int32_t j = 0; j < cmd_list->CmdBuffer.Size; j++)
		{
			// set the scissor for each UI element with new size
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[j];
			VkRect2D scissorRect;
			scissorRect.offset.x = std::max((int32_t)(pcmd->ClipRect.x), 0);
			scissorRect.offset.y = std::max((int32_t)(pcmd->ClipRect.y), 0);
			scissorRect.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
			scissorRect.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y);
			// set the scissor and draw
			vkCmdSetScissor(commandBuffer, 0, 1, &scissorRect);
			vkCmdDrawIndexed(commandBuffer, pcmd->ElemCount, 1, indexOffset, vertexOffset, 0);
			indexOffset += pcmd->ElemCount;
		}
		vertexOffset += cmd_list->VtxBuffer.Size;
	}
}

// Free all the aquired resources that are part of this class
void ImGuiClass::freeResources()
{
	// destroy the ImGui context
	ImGui::DestroyContext();

	// destroy the index, vertex buffers and memories
	vkDestroyBuffer(*device, vertexBuffer, nullptr);
	vkDestroyBuffer(*device, indexBuffer, nullptr);
	vkFreeMemory(*device, vertexDeviceMemory, nullptr);
	vkFreeMemory(*device, indexDeviceMemory, nullptr);
	
	// destroy the resources associated with the font
	vkDestroyImageView(*device, fontView, nullptr);
	vkDestroyImage(*device, fontImage, nullptr);
	vkFreeMemory(*device, fontMemory, nullptr);
	vkDestroySampler(*device, sampler, nullptr);
	// destroy the descriptor sets, pool, layouts and pipeline
	vkDestroyDescriptorSetLayout(*device, descriptorSetLayout, nullptr);
	vkDestroyDescriptorPool(*device, descriptorPool, nullptr);
	vkDestroyPipelineLayout(*device, pipelineLayout, nullptr);
	vkDestroyPipeline(*device, pipeline, nullptr);
}

// Implement a new frame for the UI
void ImGuiClass::newFrame()
{
	// use the helper fnction from ImGui to initialise the frame
	ImGui_ImplGlfw_NewFrame();
	// create a new frame
	ImGui::NewFrame();
	// start a new node, with settings as title
	ImGui::Begin("Settings");
	// use a fixed width
	ImGui::PushItemWidth(ImGui::GetFontSize() * 15);
	// add checkboxes for the bools
	ImGui::Checkbox("Model", &uiSettings.displayModels);
	ImGui::Checkbox("Texture", &uiSettings.displayTexture);
	ImGui::Checkbox("Phong shading", &uiSettings.displayPhong);
	
	// add float draggers for model's position, rotation and light's position
	{
		ImGui::PushID(0);
		ImGui::Text("Model position");
		ImGui::DragFloat3("", glm::value_ptr(uiSettings.modelPosition), 0.01f);
		ImGui::PopID();
	}
	
	{
		ImGui::PushID(1);
		ImGui::Text("Model rotation");
		ImGui::DragFloat3("", glm::value_ptr(uiSettings.modelRotation), 0.1f);
		ImGui::PopID();
	}

	{
		ImGui::PushID(2);
		ImGui::Text("Light position");
		ImGui::DragFloat3("", glm::value_ptr(uiSettings.lightPosition), 0.1f);
		ImGui::PopID();
	}
	
	// end the settings node and render
	ImGui::End();
	ImGui::Render();
}
