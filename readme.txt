Coursework 1 Submission
------------------------

The code developed for this application can be found in:
- main.cpp - which runs the application;
- modelRender.h/cpp - sets up the Vulkan and renders the duck;
- imGuiClass.h/cpp - sets up the required resources for the UI;
- vkUtils.h/cpp - general Vulkan helper functions to be used in both
modelRender and imGuiClass.
The other imgui files are required for the UI implementation.
External libraries used: glm, stbi_image, tiny_obj_loader, glfw
and imgui.

Also, there are two pairs of vertex and fragment shaders, one for
the model renderer and one for the user interface. They have to be 
compiled to .spv and this can be done using the provided `compile.bat`
script.

The implementation has been heavily aided by the Vulkan tutorial and
the code example from Sascha Willems repository.