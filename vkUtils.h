#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glm/gtx/hash.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stb_image.h>

#include <tiny_obj_loader.h>

#include "imgui.h"
#include "imgui_impl_glfw.h"

#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <optional>
#include <cstdint>
#include <algorithm>
#include <fstream>
#include <array>
#include <chrono>

// paths fro model, material and texture
const std::string MODEL_PATH = "models/duck.obj";
const std::string MATERIAL_PATH = "models/";
const std::string TEXTURE_PATH = "textures/duck.jpg";

// validation layers and required extension
const std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };
const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

// uniform buffer object to keep the uniform for the model
struct UniformBufferObject {
	alignas(16) glm::mat4 model;
	alignas(16) glm::mat4 view;
	alignas(16) glm::mat4 proj;
	alignas(16) glm::mat4 normal;
	alignas(16) glm::vec4 lightPos;
	alignas(16) glm::vec4 states;
	alignas(16) glm::vec4 ambientColor;
	alignas(16) glm::vec4 diffuseColor;
	alignas(16) glm::vec4 specularColor;
};

// vertex struct to keep a 3d vertex and its attributes
struct Vertex {
	glm::vec3 pos;
	glm::vec3 color;
	glm::vec3 normal;
	glm::vec2 texCoord;

	// get the binding description for the vertex
	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription;
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}
	// get the attribute description of all elements of the vector struct
	static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 4> attributeDescriptions{};

		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset = offsetof(Vertex, pos);

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(Vertex, color);

		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(Vertex, normal);

		attributeDescriptions[3].binding = 0;
		attributeDescriptions[3].location = 3;
		attributeDescriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[3].offset = offsetof(Vertex, texCoord);

		return attributeDescriptions;
	}

	// overloaded operator to check if two vertices are the same
	bool operator==(const Vertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord && normal == other.normal;
	}
};

// custom hashing for the vertices
namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

// struct to keep the indices of graphics and present family queues
struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;
	// check if both families have a value set
	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

// a struct to keep the swapchain details, capabilities, formats and presentModes
struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

// enable/disable the validation layers
#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

// check if the result is successful
void CheckResult(VkResult result);
// create the debugging messenger
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);

// destroy the debugging messenger
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

// find the supported formats
VkFormat findSupportedFormat(VkPhysicalDevice& physicalDevice, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

// check what swapchain capabilities are supported
SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR& surface);

// create a shader module from read in data
VkShaderModule createShaderModule(const std::vector<char>& code, VkDevice& device);

// rate the devices for suitability
int rateDeviceSuitability(VkPhysicalDevice& device);

// check if validation layers are supported
bool checkValidationLayerSupport();

// returns a command buffer for a single time command
VkCommandBuffer beginSingleTimeCommands(VkDevice& device, VkCommandPool& commandPool);

// check for format compatible with stencil
bool hasStencilComponent(VkFormat format);

// create a texture sampler
void createTextureSampler(VkPhysicalDevice& physicalDevice, VkDevice& device, VkSampler& sampler);

// create an image view
VkImageView createImageView(VkDevice& device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);

// choose swap surface format from available formats
VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);

// end a single time command buffer
void endSingleTimeCommands(VkCommandBuffer& commandBuffer, VkQueue& queue, VkDevice& device, VkCommandPool& commandPool);

// copy a buffer from src to dst
void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkDevice& device, VkQueue& queue, VkCommandPool& commandPool);

// find memory type
uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, VkPhysicalDevice& physicalDevice);

// create a buffer of given size and usage
void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkDevice& device, VkPhysicalDevice& physicalDevice);

// create a vertex buffer
void createVertexBuffer(std::vector<Vertex>& vertices, VkDevice& device, VkPhysicalDevice& physicalDevice, VkBuffer& vertexBuffer, VkDeviceMemory& vertexBufferMemory, VkCommandPool& commandPool, VkQueue& queue);

// find queue families
QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR& surface);

// get the required extensions for the device
std::vector<const char*> getRequiredExtensions();

// check is extensions are supported for given device
bool checkDeviceExtensionSupport(VkPhysicalDevice device);

// choose swap present mode from available ones
VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);

// read in a file
std::vector<char> readFile(const std::string& filename);

// load an obj model
void loadModel(std::vector<Vertex>& vertices, std::vector<uint32_t>& indices, UniformBufferObject& defaultUBO);

// create an index buffer of given size
void createIndexBuffer(std::vector<uint32_t> indices, VkDevice& device, VkPhysicalDevice& physicalDevice, VkBuffer& indexBuffer, VkDeviceMemory& indexBufferMemory, VkCommandPool& commandPool, VkQueue& queue);

// copy a buffer to image
void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue);

// create image
void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory, VkDevice& device, VkPhysicalDevice& physicalDevice);

// transition the layout of an image
void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkDevice& device, VkCommandPool& commandPool, VkQueue& queue);

// check is the device is suitable
bool isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR& surface);