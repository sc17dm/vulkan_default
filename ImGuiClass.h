#pragma once

#include "vkUtils.h"

// Struct to keep the variables that are modified by the UI
struct UISettings {
	// bool to display the duck
	bool displayModels = true;
	// bool to display texture on duck
	bool displayTexture = true;
	// bool to display Phong shading
	bool displayPhong = true;
	// the position of the duck
	glm::vec3 modelPosition;
	// the rotation of the duck
	glm::vec3 modelRotation;
	// light position
	glm::vec3 lightPosition;

};

// Class that implements the UI using ImGui
class ImGuiClass {
public:
	// pointer to the vk device to use
	VkDevice* device;
	// physical device pointer to use
	VkPhysicalDevice* physicalDevice;
	VkQueue queue;
	// don't need mip map on UI
	VkSampleCountFlagBits rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	// only using one subpass
	uint32_t subpass = 0;
	
	// index and vertex buffers for the UI drawing,
	// along with their size and memory for them
	VkBuffer indexBuffer;
	VkBuffer vertexBuffer;
	VkDeviceMemory vertexDeviceMemory;
	VkDeviceMemory indexDeviceMemory;
	int32_t vertexCount;
	int32_t indexCount;
	
	VkCommandPool commandPool;

	// shaders to use on the drawn UI
	std::vector<VkPipelineShaderStageCreateInfo> shaders;

	VkDescriptorPool descriptorPool;
	VkDescriptorSetLayout descriptorSetLayout;
	VkDescriptorSet descriptorSet;
	VkPipelineLayout pipelineLayout;
	VkPipeline pipeline;

	// font for UI, with memory, image and sampler
	VkDeviceMemory fontMemory = VK_NULL_HANDLE;
	VkImage fontImage = VK_NULL_HANDLE;
	VkImageView fontView = VK_NULL_HANDLE;
	VkSampler sampler;
	// struct to keep all settings
	UISettings uiSettings;

	// sizes of the font image
	int texWidth, texHeight;

	// data on the CPU for font, indices and vertices
	unsigned char* fontData;
	void* vertexData = nullptr;
	void* indexData = nullptr;

	// push constants for the scale and translate of each UI element
	struct PushConstBlock {
		glm::vec2 scale;
		glm::vec2 translate;
	} pushConstBlock;
	// fixed scale of the UI
	float scale = 1.0f;

	// constructor & destructor for the class
	ImGuiClass();
	~ImGuiClass();

	// prepare the pipeline for rendering
	void preparePipeline(const VkPipelineCache pipelineCache, const VkRenderPass& renderPass);
	// prepare the required resources
	void prepareResources();
	// update the buffers
	bool update();
	// draw a frame
	void draw(const VkCommandBuffer commandBuffer);
	// free all the used resources
	void freeResources();
	// move the UI to a new frame
	void newFrame();
};